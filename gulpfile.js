const {
    src,
    dest,
    series,
    watch,
    task
} = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const postcss = require("gulp-postcss");
const cssnano = require("cssnano");
const terser = require("gulp-terser");
const browsersync = require("browser-sync");
const del = require("del");

const sync = browsersync.create();

function server() {
    return sync.init({
        injectChanges: true,
        server: "dist"
    })
}

function refresh(done) {
    sync.reload();
    done();
}

task("sass", series(
    function sassCompile() {
        return src("src/sass/main.scss", {
                sourcemaps: true
            })
            .pipe(sass().on("error", sass.logError))
            .pipe(postcss([cssnano()]))
            .pipe(dest("dist/css", {
                sourcemaps: "."
            }));
    },
    refresh));

task("js", series(
    function js() {
        return src("src/js/**/*.js", {
                sourcemaps: true
            })
            .pipe(terser())
            .pipe(dest("dist/js", {
                sourcemaps: "."
            }));
    },
    refresh
));

task("static", series(
    function moveHtml() {
        return src("src/**/*.html").pipe(dest("dist"));
    },
    function moveJSON() {
        return src("src/js/*.json").pipe(dest("dist/js"));
    },
    function moveImages() {
        return src("src/assets/**/*").pipe(dest("dist/assets"));
    },
    function moveFonts() {
        return src("src/sass/fonts/**/*").pipe(dest("dist/css/fonts"));
    },
    refresh
));

task("clean", () => {
    return del("dist");
});

task("build", series(["clean", "sass", "js", "static"]));

task("default", series(["build"]));

task("watch", series(["default"], function watchChanges() {
    watch("src/sass/*.scss", series(["sass"]));
    watch("src/js/**/*.js", series(["js"]));
    watch("src/*.html", series(["static"]));
    return server();
}));