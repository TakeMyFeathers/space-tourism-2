const data = {
    "destinations": [{
            "name": "Moon",
            "images": {
                "png": "./assets/destination/image-moon.png",
                "webp": "./assets/destination/image-moon.webp"
            },
            "description": "See our planet as you’ve never seen it before. A perfect relaxing trip away to help regain perspective and come back refreshed. While you’re there, take in some history by visiting the Luna 2 and Apollo 11 landing sites.",
            "distance": "384,400 km",
            "travel": "3 days"
        },
        {
            "name": "Mars",
            "images": {
                "png": "./assets/destination/image-mars.png",
                "webp": "./assets/destination/image-mars.webp"
            },
            "description": "Don’t forget to pack your hiking boots. You’ll need them to tackle Olympus Mons, the tallest planetary mountain in our solar system. It’s two and a half times the size of Everest!",
            "distance": "225 mil. km",
            "travel": "9 months"
        },
        {
            "name": "Europa",
            "images": {
                "png": "./assets/destination/image-europa.png",
                "webp": "./assets/destination/image-europa.webp"
            },
            "description": "The smallest of the four Galilean moons orbiting Jupiter, Europa is a winter lover’s dream. With an icy surface, it’s perfect for a bit of ice skating, curling, hockey, or simple relaxation in your snug wintery cabin.",
            "distance": "628 mil. km",
            "travel": "3 years"
        },
        {
            "name": "Titan",
            "images": {
                "png": "./assets/destination/image-titan.png",
                "webp": "./assets/destination/image-titan.webp"
            },
            "description": "The only moon known to have a dense atmosphere other than Earth, Titan is a home away from home (just a few hundred degrees colder!). As a bonus, you get striking views of the Rings of Saturn.",
            "distance": "1.6 bil. km",
            "travel": "7 years"
        }
    ]
}
const destinations = document.querySelector(".destination__list").childNodes;
const image = document.querySelector("#planet-image");
const title = document.querySelector("#planet-name");
const text = document.querySelector("#planet-description");
const avgDistance = document.querySelector("#avg-distance");
const travelTime = document.querySelector("#travel-time");

destinations.forEach(dest => {
    dest.addEventListener("click", (e) => {
        const planet = e.target.name;
        const active = document.querySelector(".active");
        active.classList.remove("active");
        e.target.classList.add("active");
    
        switch (planet) {
            case "moon":
                image.src = data.destinations[0].images.webp;
                title.textContent = data.destinations[0].name
                text.textContent = data.destinations[0].description;
                avgDistance.textContent = data.destinations[0].distance;
                travelTime.textContent = data.destinations[0].travel;
                break;
            case "mars":
                image.src = data.destinations[1].images.webp;
                title.textContent = data.destinations[1].name
                text.textContent = data.destinations[1].description;
                avgDistance.textContent = data.destinations[1].distance;
                travelTime.textContent = data.destinations[1].travel;
                break;
            case "europa":
                image.src = data.destinations[2].images.webp;
                title.textContent = data.destinations[2].name
                text.textContent = data.destinations[2].description;
                avgDistance.textContent = data.destinations[2].distance;
                travelTime.textContent = data.destinations[2].travel;
                break;
            case "titan":
                image.src = data.destinations[3].images.webp;
                title.textContent = data.destinations[3].name
                text.textContent = data.destinations[3].description;
                avgDistance.textContent = data.destinations[3].distance;
                travelTime.textContent = data.destinations[3].travel;
                break;
        }
    })
})