const menu = document.querySelector(".header__toggle");
const nav = document.querySelector("nav");

menu.addEventListener("click", ()=>{
    if(nav.classList.contains("open")){
        nav.classList.remove("open");
        menu.src="assets/shared/icon-hamburger.svg";
    }else{
        nav.classList.add("open");
        menu.src="assets/shared/icon-close.svg";
    }
})